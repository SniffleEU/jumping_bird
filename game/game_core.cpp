#include "game_core.h"

game_core::game_core()
{

}

game_core::~game_core()
{
    stop();
}

void game_core::start_game()
{
    if(init()){
        run();
    } else
    {
        SDL_Log("Failed to initialize!\n");
    }
}

bool game_core::init()
{
    bool success = true;
    success = game_window_manager.init(SCREEN_WIDTH,SCREEN_HEIGHT,GAME_TITLE);
    load_stuff();
    return success;

}

void game_core::stop()
{
    player.free();
    game_window_manager.stop();
}

void game_core::run()
{
    bool quit = false;

    SDL_Event e;

    timer_manager.deltatime_timer_start();

    Uint64 NOW = SDL_GetPerformanceCounter();
    Uint64 LAST = 0;
    float deltatime = 0;

    while( !quit )
    {
        timer_manager.fps_cap_timer_start();

        while( SDL_PollEvent( &e ) != 0 )
        {
           if( e.type == SDL_QUIT )
           {
               quit = true;
           }
        }

       LAST = NOW;
       NOW = SDL_GetPerformanceCounter();

       deltatime = static_cast<float>(((NOW - LAST)*1000 / SDL_GetPerformanceFrequency() )) * static_cast<float>(0.001);
       const Uint8* currentKeyStates = SDL_GetKeyboardState( nullptr );
       if( currentKeyStates[ SDL_SCANCODE_UP ] )
       {
           player.move_position(0,-200 * deltatime);
       }
       if( currentKeyStates[ SDL_SCANCODE_DOWN ] )
       {
           player.move_position(0,200 * deltatime);
       }
       if( currentKeyStates[ SDL_SCANCODE_LEFT ] )
       {
           player.move_position(-200 * deltatime,0);
       }
       if( currentKeyStates[ SDL_SCANCODE_RIGHT ] )
       {
           player.move_position(200 * deltatime,0);
       }

        game_window_manager.clear();
        player.render_sprite(game_window_manager.get_renderer_pointer());
        game_window_manager.update();

        timer_manager.deltatime_timer_start();
        timer_manager.fps_cap_timer_delay();
     }
    stop();
}

//TODO: Replace with real loading and object creation
void game_core::load_stuff()
{
    player.create_sprite_texture("res/img/player.png", game_window_manager.get_renderer_pointer());
    player.set_position(320,240);
}
