#ifndef GAME_CORE_H
#define GAME_CORE_H

#include <string>
#include <SDL2/SDL.h>

#include "engine/window_manager.h"
#include "engine/utils/timer_general_manager.h"
#include "engine/game_objects/game_sprite.h"


#include "engine/utils/game_timer.h"


class game_core
{
public:
    game_core();
    ~game_core();

    void start_game();


private:
    const int SCREEN_WIDTH = 640;
    const int SCREEN_HEIGHT = 480;
    const std::string GAME_TITLE = "Jumping Bird";


    timer_general_manager timer_manager;
    window_manager game_window_manager;

    game_sprite player;

    bool init();
    void stop();
    void run();


    //TODO: Add stuff for testing / removal here:
    void load_stuff();


};

#endif // GAME_CORE_H
