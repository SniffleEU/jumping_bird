#ifndef WINDOW_MANAGER_H
#define WINDOW_MANAGER_H

#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

class window_manager
{
public:
    window_manager();
    virtual ~window_manager();

    bool init(int SCREEN_WIDTH, int SCREEN_HEIGHT, std::string TITLE);
    void stop();
    void clear();
    void update();

    SDL_Renderer* get_renderer_pointer(){return game_renderer;}
    SDL_Window* get_window_pointer(){return game_window;}


private:
    SDL_Window* game_window = nullptr;
    SDL_Renderer* game_renderer = nullptr;

    bool create_window_icon();

};

#endif // WINDOW_MANAGER_H
