#include "game_sprite.h"

game_sprite::game_sprite()
{

}

game_sprite::~game_sprite()
{
    free();
}

bool game_sprite::create_sprite_texture(std::string path, SDL_Renderer *g_renderer)
{
    return texture.load_texture(path, g_renderer);
}

void game_sprite::render_sprite(SDL_Renderer *game_renderer, SDL_Rect *clip)
{
    texture.render(static_cast<int>(position_x),static_cast<int>(position_y), game_renderer, clip);
}

void game_sprite::free()
{
    texture.free();
}

void game_sprite::set_position(float x, float y)
{
    position_x = x;
    position_y = y;
}

void game_sprite::move_position(float x, float y)
{
    position_x += x;
    position_y += y;
}
