#ifndef GAME_SPRITE_H
#define GAME_SPRITE_H

#include <string>
#include <SDL2/SDL.h>

#include "engine/graphics/game_texture.h"

class game_sprite
{
public:
    game_sprite();
    virtual ~game_sprite();

    bool create_sprite_texture(std::string path, SDL_Renderer* g_renderer);
    void render_sprite(SDL_Renderer* game_renderer, SDL_Rect *clip = nullptr);
    void free();

    void set_position(float x, float y);
    void move_position(float x, float y);

private:
    game_texture texture;

    float position_x, position_y;
};

#endif // GAME_SPRITE_H
