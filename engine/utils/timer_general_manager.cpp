#include "timer_general_manager.h"

timer_general_manager::timer_general_manager()
{

}

void timer_general_manager::fps_cap_timer_delay()
{
    int frameTicks = static_cast<int>(fps_cap_timer.getTicks());
    if( frameTicks < SCREEN_TICK_PER_FRAME )
    {
        SDL_Delay( static_cast<Uint32>(SCREEN_TICK_PER_FRAME - frameTicks) );
    }
}

