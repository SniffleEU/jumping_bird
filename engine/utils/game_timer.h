#ifndef GAME_TIMER_H
#define GAME_TIMER_H

#include <SDL2/SDL.h>


class game_timer
{
public:
    game_timer();

    void start();
    void stop();
    void pause();
    void unpause();

    Uint32 getTicks();

    bool isStarted(){return mStarted;}
    bool isPaused(){return mPaused && mStarted;}

private:
    Uint32 mStartTicks;
    Uint32 mPausedTicks;

    bool mPaused;
    bool mStarted;

};

#endif //GAME_TIMER_H
