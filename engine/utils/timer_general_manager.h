#ifndef GAME_FPS_LIMITER_H
#define GAME_FPS_LIMITER_H

#include <SDL2/SDL.h>

#include "engine/utils/game_timer.h"

class timer_general_manager
{
public:
    timer_general_manager();

    //Functions for fps cap timer
    void fps_cap_timer_start(){deltatime_timer.start();}
    void pause_fps_cap_timer(){deltatime_timer.pause();}
    void unpause_fps_cap_timer(){deltatime_timer.unpause();}
    void fps_cap_timer_delay();

    //TODO: REMOVE? NOT IN USE ATM --Functions for deltatime timer)
    void deltatime_timer_start(){deltatime_timer.start();}
    void pause_deltatime_timer(){deltatime_timer.pause();}
    void unpause_deltatime_timer(){deltatime_timer.unpause();}
    Uint32 deltatime_timer_get_ticks(){return deltatime_timer.getTicks(); }


private:
    const int SCREEN_FPS = 120;
    const int SCREEN_TICK_PER_FRAME = 1000 / SCREEN_FPS;

    game_timer fps_cap_timer;
    game_timer deltatime_timer;
};

#endif // GAME_FPS_LIMITER_H
