#ifndef GAME_TEXTURE_H
#define GAME_TEXTURE_H

#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

class game_texture
{
public:
    game_texture();
    ~game_texture();

    bool load_texture(std::string path, SDL_Renderer* g_renderer);
    void free();
    void render(int x, int y, SDL_Renderer* game_renderer, SDL_Rect* clip = nullptr, double angle = 0.0, SDL_Point* center = nullptr, SDL_RendererFlip flip = SDL_FLIP_NONE );

    int get_width(){return t_width;}
    int get_height(){return t_height;}

private:
    SDL_Texture* g_texture =  nullptr;

    int t_height = 0, t_width = 0;
};

#endif // GAME_TEXTURE_H
