#include "game_texture.h"

game_texture::game_texture()
{
}

game_texture::~game_texture()
{
    free();
}

bool game_texture::load_texture(std::string path, SDL_Renderer* g_renderer)
{
    free();

    SDL_Texture* n_texture = nullptr;
    SDL_Surface* l_surface = IMG_Load(path.c_str());

    if( l_surface == nullptr )
        {
            SDL_Log( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
        }
        else
        {
            SDL_SetColorKey( l_surface, SDL_TRUE, SDL_MapRGB( l_surface->format, 0, 0xFF, 0xFF ) );
            n_texture = SDL_CreateTextureFromSurface( g_renderer, l_surface );

            if( n_texture == nullptr )
            {
                SDL_Log( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
            }
            else
            {
                t_width = l_surface->w;
                t_height = l_surface->h;
            }
            SDL_FreeSurface( l_surface );
        }
        g_texture = n_texture;

        return g_texture != nullptr;
}

void game_texture::free()
{
    if(g_texture != nullptr)
    {
        SDL_DestroyTexture(g_texture);
        g_texture = nullptr;
        t_height = t_width = 0;
    }
}

void game_texture::render(int x, int y, SDL_Renderer* game_renderer, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip )
{
        SDL_Rect renderQuad = { x, y, t_width, t_height };

        if( clip != nullptr )
        {
            renderQuad.w = clip->w;
            renderQuad.h = clip->h;
        }

        SDL_RenderCopyEx(game_renderer,g_texture, nullptr,&renderQuad,angle,center,flip);
}
