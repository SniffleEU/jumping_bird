#include "game/game_core.h"

int main(int argc, char* args[])
{
    game_core core;

    core.start_game();

    return 0;
}
